#pragma once

struct options {
	int port;
	int iocp;
	int verbose;
};

struct options parse_opts(int argc, char **argv);
