#include "mime.h"

#include <initializer_list>
#include <list>
#include <memory>

using std::string;
using std::list;
using std::shared_ptr;

class entry_t
{
public:
    const string mime;
    list<string> exts;

public:
    entry_t(const string& mime) : mime(mime) {}

    entry_t& add(const string& ext) {
        exts.push_back(ext);
        return *this;
    }

    bool contains(const string& in_ext) const {
        for (auto& ext : exts) {
            if (ext == in_ext) return true;
        }
        return false;
    }
};

static string default_mime = "application/octet-stream";

static list<shared_ptr<entry_t>> map;

static void
mime_init_one(const char * mime, std::initializer_list<string> exts) {
    shared_ptr<entry_t> pentry = std::make_shared<entry_t>(string(mime));
    map.push_back(pentry);

    entry_t& entry = *pentry;
    for (const string& ext : exts) {
        entry.add(ext);
    }
}

void
mime_init() {
    mime_init_one("text/html", {
        "html",
        "htm",
    });
    
    mime_init_one("text/css", {
        "css",
    });
    
    mime_init_one("text/plain", {
        "txt",
    });
    
    mime_init_one("image/jpeg", {
        "jpeg",
        "jpg",
    });
    
    mime_init_one("image/png", {
        "png",
    });
    
    mime_init_one("image/gif", {
        "gif",
    });
    
    mime_init_one("application/pdf", {
        "pdf",
    });
}

string
mime_get(const string& ext) {
    for (auto item : map) {
        if (item->contains(ext)) {
            return item->mime;
        }
    }
    return default_mime;
}
