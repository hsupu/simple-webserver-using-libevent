#pragma once

#include <string>

void mime_init();
std::string mime_get(const std::string& ext);
