#include "parse_opts.h"
#include "server.h"

int
main(int argc, char **argv, char **envp) {
    struct options opts = parse_opts(argc, argv);
    server_init(opts);
    return 0;
}
