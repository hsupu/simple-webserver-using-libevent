#include "handlers.h"

#include <event2/buffer.h>
#include <event2/keyvalq_struct.h>

#include <cstdio>
#include <memory>

using std::shared_ptr;

static void
handle_dump(struct evhttp_request *req, void *cb_arg) {
	const char *cmdtype;

	switch (evhttp_request_get_command(req)) {
        case EVHTTP_REQ_GET: cmdtype = "GET"; break;
        case EVHTTP_REQ_POST: cmdtype = "POST"; break;
        case EVHTTP_REQ_HEAD: cmdtype = "HEAD"; break;
        case EVHTTP_REQ_PUT: cmdtype = "PUT"; break;
        case EVHTTP_REQ_DELETE: cmdtype = "DELETE"; break;
        case EVHTTP_REQ_OPTIONS: cmdtype = "OPTIONS"; break;
        case EVHTTP_REQ_TRACE: cmdtype = "TRACE"; break;
        case EVHTTP_REQ_CONNECT: cmdtype = "CONNECT"; break;
        case EVHTTP_REQ_PATCH: cmdtype = "PATCH"; break;
        default: cmdtype = "unknown"; break;
	}

	printf("%s %s\n", cmdtype, evhttp_request_get_uri(req));

	struct evkeyvalq *headers = evhttp_request_get_input_headers(req);
	for (struct evkeyval *header = headers->tqh_first; header; header = header->next.tqe_next) {
		printf("  %s: %s\n", header->key, header->value);
	}

	struct evbuffer *buf;
	buf = evhttp_request_get_input_buffer(req);
	puts("<<<");
	while (evbuffer_get_length(buf)) {
		int n;
		char cbuf[4096];
		n = evbuffer_remove(buf, cbuf, sizeof(cbuf));
		if (n > 0)
			(void) fwrite(cbuf, 1, n, stdout);
	}
	puts(">>>");

	evhttp_send_reply(req, 200, "OK", NULL);
}

static shared_ptr<struct evbuffer>
allocate_evbuffer() {
	shared_ptr<struct evbuffer> buf(
		evbuffer_new(),
		[](auto buf) -> void { evbuffer_free(buf); }
	);
	return buf;
}

static void
handle_default(struct evhttp_request *req, void *cb_arg) {
	shared_ptr<struct evbuffer> respbuf = allocate_evbuffer();
	if (!respbuf) {
		fprintf(stderr, "allocate evbuffer failed.\n");
		return;
	}
	evbuffer_add_printf(respbuf.get(), "Hello World.\n%s\n", evhttp_request_get_uri(req));
	evhttp_send_reply(req, 200, "OK", respbuf.get());
}

void
set_http_callbacks(struct evhttp *http) {
	// specified path handler
	evhttp_set_cb(http, "/dump", handle_dump, NULL);
	// default handler
	evhttp_set_gencb(http, handle_default, NULL);
}
