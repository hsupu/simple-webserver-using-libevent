#include "parse_opts.h"

#ifdef _WIN32
#include <getopt.h>
#endif /* _WIN32 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void
print_usage(FILE *out, const char *prog, int exit_code) {
	fprintf(out, "Syntax: [ OPTS ] %s <webroot>\n", prog);
	fprintf(out, " -p  - port\n");
	fprintf(out, " -I  - IOCP\n");
	fprintf(out, " -v  - verbosity: enables libevent debug logging\n");
	exit(exit_code);
}

struct options
parse_opts(int argc, char **argv) {
	struct options o;
	int opt;

	memset(&o, 0, sizeof(o));

	while ((opt = getopt(argc, argv, "hp:Iv")) != -1) {
		switch (opt) {
			case 'p': o.port = atoi(optarg); break;
			case 'I': o.iocp = 1; break;
			case 'v': ++o.verbose; break;
			case 'h': print_usage(stdout, argv[0], 0); break;
			default : fprintf(stderr, "unknown option %c\n", opt); break;
		}
	}

	if (optind >= argc || (argc-optind) > 1) {
		print_usage(stdout, argv[0], 1);
	}

	return o;
}
