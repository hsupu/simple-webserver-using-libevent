#include "server.h"

#include "handlers.h"
#include "parse_opts.h"

#include <event2/event.h>
#include <event2/http.h>
#include <event2/listener.h>
#ifdef _WIN32
#include <event2/thread.h>
#endif /* _WIN32 */
#include <event2/util.h>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <sys/socket.h>
#endif /* _WIN32 */

#include <cstdio>
#include <cstdlib>
#include <memory>

using std::make_shared;
using std::shared_ptr;

static int
print_socket_info(struct evhttp_bound_socket *handle) {
	evutil_socket_t fd = evhttp_bound_socket_get_fd(handle);
	struct sockaddr_storage ss;
	ev_socklen_t socklen = sizeof(ss);
	memset(&ss, 0, sizeof(ss));
	if (getsockname(fd, (struct sockaddr *)&ss, &socklen)) {
		perror("getsockname");
		return -1;
	}

	void *inaddr;
	int port = -1;

	if (ss.ss_family == AF_INET) {
		port = ntohs(((struct sockaddr_in*)&ss)->sin_port);
		inaddr = &((struct sockaddr_in*)&ss)->sin_addr;
	} else if (ss.ss_family == AF_INET6) {
		port = ntohs(((struct sockaddr_in6*)&ss)->sin6_port);
		inaddr = &((struct sockaddr_in6*)&ss)->sin6_addr;
	} else {
		fprintf(stderr, "Unhandled address family %d\n", ss.ss_family);
		return -2;
	}

	char addrbuf[128];
	const char *addr;
	addr = evutil_inet_ntop(ss.ss_family, inaddr, addrbuf, sizeof(addrbuf));
	if (!addr) {
		fprintf(stderr, "evutil_inet_ntop() failed\n");
		return -3;
	}

	printf("Listening on http://%s:%d\n", addr, port);
	return 0;
}

static shared_ptr<struct event_config>
init_event_config(struct options &opts) {
	shared_ptr<struct event_config> cfg(
		event_config_new(),
		[](auto cfg) -> void { event_config_free(cfg); }
	);
#ifdef _WIN32
	// IOCP is a feature on Windows only
	if (opts.iocp) {
	#ifdef EVTHREAD_USE_WINDOWS_THREADS_IMPLEMENTED
		evthread_use_windows_threads();
		event_config_set_num_cpus_hint(cfg.get(), 8);
	#endif
		event_config_set_flag(cfg.get(), EVENT_BASE_FLAG_STARTUP_IOCP);
	}
#endif
	return cfg;
}

static shared_ptr<struct event_base>
init_event_base(struct event_config *cfg) {
	shared_ptr<struct event_base> base(
		event_base_new_with_config(cfg),
		[](auto base) -> void { event_base_free(base); }
	);
	return base;
}

static shared_ptr<struct evhttp>
create_http(struct event_base *base) {
	shared_ptr<struct evhttp> http(
		evhttp_new(base),
		[](auto http) -> void { evhttp_free(http); }
	);
	return http;
}

static void
handle_signals(evutil_socket_t sig, short events, void *arg) {
	auto base = static_cast<struct event_base *>(arg);
	fprintf(stderr, "received %i\n", sig);
	event_base_loopbreak(base);
}

static shared_ptr<struct event>
register_signal(struct event_base *base) {
	event_callback_fn cb = handle_signals;
	shared_ptr<struct event> ev(
		evsignal_new(base, SIGINT, cb, base),
		[](auto ev) -> void { event_free(ev); }
	);
	if (ev) {
		event_add(ev.get(), NULL);
	}
	return ev;
}

int
server_init(struct options opts) {
#ifdef _WIN32
	// check winsock version (requires >= 2.2)
	{
		WORD wVersionRequested;
		WSADATA wsaData;
		wVersionRequested = MAKEWORD(2, 2);
		WSAStartup(wVersionRequested, &wsaData);
	}
#endif

	// for debug
	setbuf(stdout, NULL);
	setbuf(stderr, NULL);

	if (opts.verbose || getenv("EVENT_DEBUG_LOGGING_ALL"))
		event_enable_debug_logging(EVENT_DBG_ALL);

	shared_ptr<struct event_config> cfg = init_event_config(opts);
	shared_ptr<struct event_base> base = init_event_base(cfg.get());
	if (!base) {
		fprintf(stderr, "couldn't create event_base.\n");
		return -1;
	}
	cfg.reset();

	shared_ptr<struct evhttp> http = create_http(base.get());
	if (!http) {
		fprintf(stderr, "couldn't create evhttp.\n");
		return -2;
	}

	set_http_callbacks(http.get());

	shared_ptr<struct evhttp_bound_socket> handle(
		evhttp_bind_socket_with_handle(http.get(), "0.0.0.0", opts.port),
		[](auto handle) -> void {}
	);
	if (!handle) {
		fprintf(stderr, "couldn't bind to port %d.\n", opts.port);
		return -3;
	}
	
	if (print_socket_info(handle.get())) {
		return -4;
	}

	shared_ptr<struct event> ev_signal = register_signal(base.get());
	if (!ev_signal) {
		return -5;
	}	
	
	event_base_dispatch(base.get());
	
#ifdef _WIN32
	WSACleanup();
#endif

	return 0;
}
