CC=g++ -std=c++17
AR=ar
LD=ld

CFLAGS=-m64 -ggdb -O0
LDFLAGS=

INCLUDES = \
	-I.

SOURCES = \
	src/handlers.cpp \
	src/main.cpp \
	src/mime.cpp \
	src/parse_opts.cpp \
	src/server.cpp

OBJECTS = $(SOURCES:%.cpp=%.o)

LIBRARIES = \
	-levent \
	-lws2_32

TARGET = main.exe

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(LDFLAGS) $^ $(LIBRARIES)

%.o: %.cpp
	$(CC) -c -o $@ $(CPPFLAGS) $(CFLAGS) $^ $(INCLUDES)

.PHONY: clean
clean:
	rm $(TARGET) $(OBJECTS)
